		//Momshad Hussain 1837713
	package LinearAlgebra;
		
		public class Vector3d {
			private final double x;
			private final double y;
			private final double z;
			
		public  Vector3d(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		public double getX() 
		{
			return this.x;
		}
		
		public double getY() 
		{
			return this.y;
		}
		
		
		public double getZ() 
		{
			return this.z;
		}
		
		
		public double magnitude() 
		{
			double magnitude;
			magnitude = Math.sqrt((x*x)+(y*y)+(z*z));
			return magnitude;
		}
		
		public double dotProduct(Vector3d vector2)
		{
			double dotProduct;
			dotProduct = ((this.getX()*vector2.getX()) + (this.getY()*vector2.getY()) + (this.getZ()*vector2.getZ()));
			return dotProduct;
		}
		
		public Vector3d add (Vector3d vector2)
		{
			double x2 = this.getX() + vector2.getX();
			double y2 = this.getY() + vector2.getY();
			double z2 = this.getZ() + vector2.getZ();
			
			 Vector3d vector3 = new Vector3d(x2,y2,z2);
			 return  vector3;
		}
		
		public boolean isEqual(Vector3d someVector)
		{
			if (x == someVector.getX() 
					&& (y == someVector.getY() 
					&& (z == someVector.getZ())))
			{
				return true;
			}
			return false;
			
		}
	}
