		//Momshad Hussain 1837713
	package LinearAlgebra;
		
		
		import static org.junit.Assert.*;				
		import org.junit.Test;	
		
		public class vector3dTests {
			
			@Test 
			public void testGettors() {
				Vector3d vector1 = new Vector3d(1,1,2);
				assertTrue(vector1.getX()==1.0);
				assertTrue(vector1.getY()==1.0);
				assertTrue(vector1.getZ()==2.0);
			}
			
			
			@Test
			public void testMagnitude() {
				Vector3d mathLogic = new Vector3d(1,1,2);
				assertTrue(mathLogic.magnitude()== 2.449489742783178);
			}
			
			@Test
			public void testDotProduct() {
				Vector3d vector1 = new Vector3d(1,1,2);
				Vector3d vector2 = new Vector3d(1,4,1);
				assertTrue(vector1.dotProduct(vector2)== 7.0);
			}
			
			@Test
			public void testAdd() {
				
				//parameters
				Vector3d vector1 = new Vector3d(1,1,2);
				Vector3d vector2 = new Vector3d(1,4,1);
				
				//Expected/Actual Values 
				Vector3d expectedVector = new Vector3d(2,5,3);
				Vector3d actualVector = vector1.add(vector2);
				
				//Vector3d gettors tested implicitly by is equal method
				assertTrue(expectedVector.isEqual(actualVector));
			} 
		}
