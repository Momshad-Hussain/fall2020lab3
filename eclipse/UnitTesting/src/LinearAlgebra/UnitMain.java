package LinearAlgebra;

public class UnitMain {

	public static void main(String[] args) {
		
		Vector3d vector = new Vector3d(1,1,2);
		Vector3d vector2 = new Vector3d(1,4,1);
		
	
		System.out.println(vector.magnitude());
		System.out.println(vector.dotProduct(vector2));
		System.out.println(vector.add(vector2));
	}

}
